package com.lexot.cenicafe;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements
        SurfaceHolder.Callback, SensorEventListener {
    private MediaRecorder mMediaRecorder;
    private boolean recording;
    private Camera camera = null;
    private Camera.PreviewCallback cameraPreviewCallback = null;
    private DrawingView drawingView;
    private String fileOut;
    private Long tsLong;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private Long initialTime = 0l;
    private long lastUpdate = 0;
    private String accs = ";;;";
    private String linacc = ";;;";
    private String grav = ";;;";
    private String gyrs = ";;;";
    private String rots = ";;;";
    private String gpss = ";;";
    private Sensor senGyroscope;
    private Sensor senRotation;
    FileOutputStream outputStreamWriter;
    private Sensor senAccelerometerLinear;
    private String message;
    private File outputFile;
    Handler handler = new Handler();
    // Define the code block to be executed
    Runnable runnableCode = new Runnable() {
        @Override
        public void run() {

            long curTime = System.currentTimeMillis();
            writeToFile(((Long)(curTime - lastUpdate)).toString() +";"+accs+grav+linacc+gyrs+rots+gpss,outputStreamWriter);
            handler.postDelayed(runnableCode, 10);
        }
    };

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Thread renderThread;
    private File myDirectory;

    private void writeToFile(String data,FileOutputStream stream) {
        try {
            stream.write((data+"\n").getBytes());

        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Aplicación en FullScreen

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        tsLong = System.currentTimeMillis()/1000;
        myDirectory = new File("/mnt/sdcard/data"+tsLong.toString());  // ******** <- what do I have to put HERE for standard data folder????
        myDirectory.mkdir();

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        setContentView(R.layout.activity_main);

        drawingView = new DrawingView(this);
        LayoutParams layoutParamsDrawing
                = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        this.addContentView(drawingView, layoutParamsDrawing);
        CameraSurfaceView cameraView = (CameraSurfaceView) findViewById(R.id.surfaceview);



        cameraView.getHolder().addCallback(this);

        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                gpss = location.getLatitude()+";"+location.getLongitude()+";";

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        List<Sensor> sensors = senSensorManager.getSensorList(Sensor.TYPE_ALL);
        message = "Lista de sensores:\n\n";

        for (int i = 0; i < sensors.size(); i++) {
            message += "Nombre: " + sensors.get(i).getName() + "\n";
            message += "Resolución: " + sensors.get(i).getResolution() + "\n";
            message += "Vendor: " + sensors.get(i).getVendor() + "\n";
            message += "Tipo: " + sensors.get(i).getType() + "\n\n\n";

        }
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senGyroscope = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        senRotation = senSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode,event);
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            if(recording)
            {
                camera.lock();
            }
            touchFocus();
        }
        if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            if(recording)
            {
                stopBlinkText();
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                mMediaRecorder.release();
                recording = false;
                //startPlaying();

                handler.removeCallbacks(runnableCode);
                if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    finish();
                }
                locationManager.removeUpdates(locationListener);
                senSensorManager.unregisterListener(MainActivity.this);
                try {
                    outputStreamWriter.flush();
                    outputStreamWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                emailIntent.setType("vnd.android.cursor.dir/email");
                ArrayList<Uri> uris = new ArrayList<Uri>();
                uris.add(Uri.parse("file:///mnt/sdcard/data"+tsLong.toString()+"/data.txt"));
                emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensores");
                emailIntent.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(emailIntent, "Enviando email..."));
            }
            else {
                blinkText();
                try {
                    outputFile = new File(myDirectory, "data.txt");
                    outputStreamWriter = new FileOutputStream(outputFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                lastUpdate = System.currentTimeMillis();
                handler.post(runnableCode);

                senSensorManager.registerListener(MainActivity.this, senRotation, SensorManager.SENSOR_DELAY_FASTEST);
                senSensorManager.registerListener(MainActivity.this, senAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
                senSensorManager.registerListener(MainActivity.this, senGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
                //senSensorManager.registerListener(MainActivity.this, senAccelerometerLinear, SensorManager.SENSOR_DELAY_FASTEST);
                //senSensorManager.registerListener(MainActivity.this, senGyroscopeUncalibrated, SensorManager.SENSOR_DELAY_FASTEST);
                //senSensorManager.registerListener(MainActivity.this, senGravity, SensorManager.SENSOR_DELAY_FASTEST);


                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

                //camera.unlock();
                mMediaRecorder.start();
                recording = true;
            }
            //   camera.takePicture(myShutterCallback,
            //       myPictureCallback_RAW, myPictureCallback_JPG);
        }
        return true;
    }
    public void touchFocus(){

        Rect targetFocusRect = new Rect(-1000,-600,1000,600);
        int w = drawingView.getWidth();
        int h = drawingView.getHeight();
        Rect tFocusRect = new Rect((targetFocusRect.left+1000)*w/2000,(targetFocusRect.top+1000)*h/2000,(targetFocusRect.right+1000)*w/2000,(targetFocusRect.bottom+1000)*h/2000);
        final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
        camera.cancelAutoFocus();
        Camera.Area focusArea = new Camera.Area(targetFocusRect, 1000);
        focusList.add(focusArea);
        Camera.Parameters para = camera.getParameters();

        List<String> focusModes = para.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO))
            para.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
        if (para.getMaxNumFocusAreas() > 0) {
            para.setFocusAreas(focusList);
        }
        if (para.getMaxNumMeteringAreas() > 0) {
            para.setMeteringAreas(focusList);
        }
        TextView tvRecording = (TextView) findViewById(R.id.tvRecording);
        tvRecording.setVisibility(View.INVISIBLE);
        camera.setParameters(para);
        camera.autoFocus(myAutoFocusCallback);


        drawingView.setHaveTouch(true, tFocusRect);
        drawingView.invalidate();
    }
    Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback(){

        @Override
        public void onAutoFocus(boolean arg0, Camera arg1) {
            // TODO Auto-generated method stub
            if (arg0){
                camera.cancelAutoFocus();
                Camera.Parameters params = arg1.getParameters();
                if(params.getFocusMode() != Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE){
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    arg1.setParameters(params);
                }

            }

            float focusDistances[] = new float[3];
            arg1.getParameters().getFocusDistances(focusDistances);

        }};

    private Camera.Size getBestPreviewSize(int width, int height, Camera.Parameters parameters){
        Camera.Size bestSize = null;
        List<Camera.Size> sizeList = parameters.getSupportedPreviewSizes();

        bestSize = sizeList.get(0);

        for(int i = 1; i < sizeList.size(); i++){
            if((sizeList.get(i).width * sizeList.get(i).height) >
                    (bestSize.width * bestSize.height)){
                bestSize = sizeList.get(i);
            }
        }

        return bestSize;
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        //
        if (holder.getSurface() == null) {
            return;
        }

        Camera.Parameters parameters = this.camera.getParameters();
        // If null, likely called after camera has been released.
        if (parameters == null) {
            return;
        }

        Camera.Size size = getBestPreviewSize(width,height,parameters);
        int previewWidth = width;
        int previewHeight = height;

        previewWidth = size.width;
        previewHeight = size.height;
        parameters.setPreviewSize(previewWidth, previewHeight);



        camera.setParameters(parameters);
        camera.startPreview();


        camera.unlock();
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setCamera(camera);
        //mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);



        fileOut = getOutputMediaFile().toString();

        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
        mMediaRecorder.setVideoEncodingBitRate(3000000);
        mMediaRecorder.setVideoFrameRate(16);
        mMediaRecorder.setVideoSize(previewWidth, previewHeight);
        mMediaRecorder.setOutputFile(fileOut);

        try {
            mMediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //camera.lock();
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.camera = Camera.open();
        camera.setPreviewCallback(cameraPreviewCallback);
        try {
            camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {
        camera = null;
    }


    private class DrawingView extends View {

        Paint drawingPaint;

        boolean haveTouch;
        Rect touchArea;

        public DrawingView(Context context) {
            super(context);
            drawingPaint = new Paint();
            drawingPaint.setColor(Color.GREEN);
            drawingPaint.setStyle(Paint.Style.STROKE);
            drawingPaint.setStrokeWidth(2);

            haveTouch = false;
        }

        public void setHaveTouch(boolean t, Rect tArea){
            haveTouch = t;
            touchArea = tArea;
        }

        @Override
        protected void onDraw(Canvas canvas) {

            if(haveTouch){
                drawingPaint.setColor(Color.BLUE);
                canvas.drawRect(
                        touchArea.left, touchArea.top, touchArea.right, touchArea.bottom,
                        drawingPaint);
            }
        }

    }
    private File getOutputMediaFile() {
            return new File("/mnt/sdcard/data"+tsLong.toString() + "/video.mp4");
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if(initialTime == 0)
            initialTime = System.currentTimeMillis();
        if (mySensor.getType() == Sensor.TYPE_GYROSCOPE) {
            gyrs = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";";
        }
        //if (mySensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED) {
        //    gyrsunc = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";"+((Float)sensorEvent.values[3]).toString()+";"+((Float)sensorEvent.values[4]).toString()+";"+((Float)sensorEvent.values[5]).toString()+";";
        //}
        if (mySensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            linacc = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";";
        }
        if (mySensor.getType() == Sensor.TYPE_GRAVITY) {
            grav = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";";
        }
        if (mySensor.getType() == Sensor.TYPE_ORIENTATION) {

            rots = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";";
        }
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            accs = ((Float)sensorEvent.values[0]).toString()+";"+((Float)sensorEvent.values[1]).toString()+";"+((Float)sensorEvent.values[2]).toString()+";";
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }



    private void blinkText() {

        final Handler blinkHandler = new Handler();
        renderThread = new Thread(new Runnable() {
            @Override
            public void run() {
                final int timeToBlink = 1000;    //in milissegunds
                blinkHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        TextView tvRecording = (TextView) findViewById(R.id.tvRecording);
                        while (!Thread.interrupted()) {
                            if (tvRecording.getVisibility() == View.VISIBLE) {
                                tvRecording.setVisibility(View.INVISIBLE);
                            } else {
                                tvRecording.setVisibility(View.VISIBLE);
                            }
                            try{Thread.sleep(timeToBlink);}catch (Exception e) {}
                        }
                    }
                });
            }
        });
        //renderThread.start();
    }
    private void stopBlinkText()
    {
        renderThread.interrupt();
        TextView tvRecording = (TextView) findViewById(R.id.tvRecording);
        tvRecording.setVisibility(View.INVISIBLE);
    }
}
