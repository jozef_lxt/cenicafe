package com.lexot.cenicafe;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.onesignal.OneSignal;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.json.JSONObject;

/**
 * Created by imaginamos on 13/3/2016.
 */

@ReportsCrashes(formKey = "", // will not be used
        mailTo = "jozef.lxt@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.app_name)
public class MyApplication extends MultiDexApplication {

    public String NotMessage;
    public JSONObject NotAdditionalData;
    public String NotTitle;

    @Override
    public void onCreate() {
        super.onCreate();
        // Iniciamos ACRA
        ACRA.init(this);
        Log.d("Iniciando", "Iniciando");
    }

}