package com.lexot.cenicafe;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Handler;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends Activity implements SurfaceHolder.Callback {
    private final String VIDEO_PATH_NAME = "/mnt/sdcard/cenicafe.mp4";
    private CameraSurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private boolean mInitSuccesful;
    private boolean recording;
    private MediaRecorder mMediaRecorder;
    DrawingView drawingView;
    Camera camera;
    LayoutInflater controlInflater = null;
    TextView prompt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        mSurfaceView = (CameraSurfaceView) findViewById(R.id.surfaceview);
        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        drawingView = new DrawingView(this);
        LayoutParams layoutParamsDrawing
                = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        this.addContentView(drawingView, layoutParamsDrawing);

        controlInflater = LayoutInflater.from(getBaseContext());
        View viewControl = controlInflater.inflate(R.layout.control, null);
        LayoutParams layoutParamsControl
                = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT);
        this.addContentView(viewControl, layoutParamsControl);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        try {
            if(!mInitSuccesful)
                initRecorder(mHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void initRecorder(SurfaceHolder holder) throws IOException {
        if(camera == null) {
            camera = Camera.open();
            camera.unlock();
        }
        // It is very important to unlock the camera before doing setCamera
        // or it will results in a black preview
        if (camera != null){
            try {
                camera.setPreviewDisplay(holder);
                camera.startPreview();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if(mMediaRecorder == null)  mMediaRecorder = new MediaRecorder();
        //mMediaRecorder.setPreviewDisplay(holder.getSurface());
        mMediaRecorder.setCamera(camera);

        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
        //       mMediaRecorder.setOutputFormat(8);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(640, 480);
        mMediaRecorder.setOutputFile(VIDEO_PATH_NAME);

        try {
            mMediaRecorder.prepare();
        } catch (IllegalStateException e) {
            // This is thrown if the previous calls are not called with the
            // proper order
            e.printStackTrace();
        }

        mInitSuccesful = true;
    }
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
    private void shutdown() {
        // Release MediaRecorder and especially the Camera as it's a shared
        // object that can be used by other applications

        mMediaRecorder.reset();
        mMediaRecorder.release();
        camera.release();

        // once the objects have been released they can't be reused
        mMediaRecorder = null;
        camera = null;
    }
    Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback(){

        @Override
        public void onAutoFocus(boolean arg0, Camera arg1) {
            // TODO Auto-generated method stub
            if (arg0){
                camera.cancelAutoFocus();
                Camera.Parameters params = arg1.getParameters();
                if(params.getFocusMode() != Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE){
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    prompt.setText("Area enfocada");
                    arg1.setParameters(params);
                }

            }

            float focusDistances[] = new float[3];
            arg1.getParameters().getFocusDistances(focusDistances);
            prompt.setText("Distancia de enfoque óptima: "
                    + focusDistances[Camera.Parameters.FOCUS_DISTANCE_OPTIMAL_INDEX]);

        }};

    public void touchFocus(){

        Rect targetFocusRect = new Rect(-1000,-600,1000,600);
        int w = drawingView.getWidth();
        int h = drawingView.getHeight();
        Rect tFocusRect = new Rect((targetFocusRect.left+1000)*w/2000,(targetFocusRect.top+1000)*h/2000,(targetFocusRect.right+1000)*w/2000,(targetFocusRect.bottom+1000)*h/2000);
        final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
        camera.cancelAutoFocus();
        Camera.Area focusArea = new Camera.Area(targetFocusRect, 1000);
        focusList.add(focusArea);
        Camera.Parameters para = camera.getParameters();

        List<String> focusModes = para.getSupportedFocusModes();
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO))
            para.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
        if (para.getMaxNumFocusAreas() > 0) {
            para.setFocusAreas(focusList);
        }
        if (para.getMaxNumMeteringAreas() > 0) {
            para.setMeteringAreas(focusList);
        }
        prompt.setText("Enfocando en area");
        camera.setParameters(para);
        camera.autoFocus(myAutoFocusCallback);


        drawingView.setHaveTouch(true, tFocusRect);
        drawingView.invalidate();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode,event);
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){

            touchFocus();
        }
        if(keyCode == KeyEvent.KEYCODE_VOLUME_UP)
        {
            if(recording)
            {
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                recording = false;
            }
            else {
                mMediaRecorder.start();
                recording = true;
            }
            //   camera.takePicture(myShutterCallback,
            //       myPictureCallback_RAW, myPictureCallback_JPG);
        }
        return true;
    }
    private class DrawingView extends View {

        Paint drawingPaint;

        boolean haveTouch;
        Rect touchArea;

        public DrawingView(Context context) {
            super(context);
            drawingPaint = new Paint();
            drawingPaint.setColor(Color.GREEN);
            drawingPaint.setStyle(Paint.Style.STROKE);
            drawingPaint.setStrokeWidth(2);

            haveTouch = false;
        }

        public void setHaveTouch(boolean t, Rect tArea){
            haveTouch = t;
            touchArea = tArea;
        }

        @Override
        protected void onDraw(Canvas canvas) {

            if(haveTouch){
                drawingPaint.setColor(Color.BLUE);
                canvas.drawRect(
                        touchArea.left, touchArea.top, touchArea.right, touchArea.bottom,
                        drawingPaint);
            }
        }

    }


    Camera.ShutterCallback myShutterCallback = new Camera.ShutterCallback(){

        @Override
        public void onShutter() {
            // TODO Auto-generated method stub

        }};

    Camera.PictureCallback myPictureCallback_RAW = new Camera.PictureCallback(){

        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {
            // TODO Auto-generated method stub

        }};

    Camera.PictureCallback myPictureCallback_JPG = new Camera.PictureCallback(){

        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {
            // TODO Auto-generated method stub
			/*Bitmap bitmapPicture
				= BitmapFactory.decodeByteArray(arg0, 0, arg0.length);	*/

            Uri uriTarget = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());

            OutputStream imageFileOS;
            try {
                imageFileOS = getContentResolver().openOutputStream(uriTarget);
                imageFileOS.write(arg0);
                imageFileOS.flush();
                imageFileOS.close();

                prompt.setText("Imagen guardada: " + uriTarget.toString());

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            camera.startPreview();
        }};

}
