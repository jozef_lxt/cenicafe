package com.lexot.cenicafe;


import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

public class CameraSurfaceView extends SurfaceView  {

    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Needed for older version of Android prior to 3.0
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        // TODO Auto-generated constructor stub
    }

    public CameraSurfaceView(Context context) {
        super(context);

        // Needed for older version of Android prior to 3.0
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


}